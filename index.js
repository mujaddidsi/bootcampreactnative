// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let startTime = 10000
readBooks(startTime, books[0], (sisaWaktu1)=>{
    //console.log(sisaWaktu)
    readBooks(sisaWaktu1, books[1], (sisaWaktu2) => {
        //console.log(sisaWaktu)
        readBooks(sisaWaktu2, books[2], ()=>{})
    })
})
