let readBooksPromise = require('./promise.js')

let books = [
    {name: 'LOTR', timeSpent: 9000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

starTime = 10000

readBooksPromise(starTime, books[0])
    .then((waktuSisa1)=>{
        //console.log(waktuSisa1)
        readBooksPromise(waktuSisa1, books[1])
            .then((waktuSisa3) => {
                //console.log(waktuSisa3)
                readBooksPromise(waktuSisa3, books[2])
                    .then((resolve)=> {
                        //console.log(resolve)
                    })
                    .catch((reject)=>{
                        //console.log(reject)
                    })
            })
            .catch((reject)=>{
                //console.log("Keluar promise")
            })
    })
    .catch((reject)=>{
        //console.log(reject)
    })