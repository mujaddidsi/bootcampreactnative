/*
No. 1 Looping While 
*/
/*
//Looping pertama
var i = 1;
while (i < 21) {
    if (i%2 == 0)
        console.log(i + " - I love coding");
    i++;
}
*/
//Looping kedua
var i = 20;
while (i < 1) {
    if (i%2 == 0)
        console.log(i + " - I will become a mobile developer");
    i--;
}
/*
No. 2 Looping menggunakan for
*/
for (i=0; i<21; i++) {
    if(i%2 == 1) {
        console.log(i + " - Santai")
    }
    else if(i%2 == 0) {
        console.log(i + " - Berkualitas")
    }
    else if(i%2 == 1 && i%3 == 0) {
        console.log(i + " - I Love Coding")
    }
}

/*
No. 3 Membuat Persegi Panjang #
*/
while(i < 4) {
    j = 0
    while(j < 8) {
        console.log('*', end = '  ')
        j = j + 1
    i = i + 1
    } 
    prconsole.logint()
}