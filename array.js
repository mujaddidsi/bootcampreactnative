/*
Soal No. 1 (Range) 
Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).

struktur fungsinya seperti berikut range(startNum, finishNum) {}
Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1
*/
function range(startNum, finishNum) {
    var rangeArr = [];
    var rangeLength = (startNum-finishNum)+1;
    if(startNum > finishNum) {
        var rangeLength = startNum - finishNum +1;
        for (var i =0; i< rangeLength; i++) {
            rangeArr.push(startNum - i)
        }
    }
    else if(startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1;
        for (var i = 0; i < rangeLength; i++) {
            rangeArr.push(startNum + i) 
        }
    }
    else if(!startNum || !finishNum) {
        return -1
    }
    return rangeArr
};
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

/*
Soal No. 2 (Range with Step)
Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.

struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}
*/

function rangeWithStep(startNum, finishNum, step) {
    var rangeArr = [];
    var rangeLength = (startNum-finishNum)+1;
    if(startNum > finishNum) {
        var rangeLength = startNum - finishNum +1;
        for (var i =0; i< rangeLength; i += step) {
            rangeArr.push(startNum - i)
        }
    }
    else if(startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1;
        for (var i = 0; i < rangeLength; i += step) {
            rangeArr.push(startNum + i) 
        }
    }
    else if(!startNum || !finishNum) {
        return -1
    }
    return rangeArr
};
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

/*
Soal No. 3 (Sum of Range)
Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal sebelumnya. Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan soal ini.

Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. contohnya sum(1,10,1) akan menghasilkan nilai 55.

ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.
*/
function sum(startNum, finishNum, step) {
    var rangeArr = [];
    var rangeLength = (startNum-finishNum)+1;
    var total = 0;
    if(startNum == null && finishNum == null) {
        return 0
    }
    else if(startNum > finishNum) {
        var rangeLength = startNum - finishNum +1;
        if (!step){
            step = 1
        }
        for (var i =0; i< rangeLength; i += step) {
            rangeArr.push(startNum - i)
        }
        for (var i=0; i<rangeArr.length; i++) { 
            total += rangeArr[i] }
    }
    else if(startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1;
        if (!step){
            step = 1
        }
        for (var i = 0; i < rangeLength; i += step) {
            rangeArr.push(startNum + i) 
        }
        for (var i=0; i<rangeArr.length; i++) {
             total += rangeArr[i] }
    }
    else if(!finishNum) {
        return startNum
    }
    return total
};

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
