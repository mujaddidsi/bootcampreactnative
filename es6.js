/*
Petunjuk

Ubahlah Code berikut dari ES5 ke ES6:

1. Mengubah fungsi menjadi fungsi arrow

const golden = function goldenFunction(){
  console.log("this is golden!!")
}
golden();
*/
const golden = goldenFunction = () => {
    console.log("this is golden!!");
}
golden();

/*
2. Sederhanakan menjadi Object literal di ES6
return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, 
ubahlah menjadi bentuk yang lebih sederhana di ES6.

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
*/
const literal = (first, lastName) => {
  return {
       first,
       lastName,
      'fullName'(){
          console.log(`${this.first} ${this.lastName}`)
          // return `${this.first}  ${this.lastName}`
      }
  }
}

// 3. Destructuring
/*
The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays,
or properties from objects, into distinct variables.
 */
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

// destructuring
let {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation, spell)
console.log()

//4. Array Spreading
/*
Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

 */

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//Spread syntax (...)
const combined = [...west, ...east]

console.log(combined)

// 5. Template Literals
/*
sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

// Driver Code
console.log(before)

 */

const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, 
 ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)