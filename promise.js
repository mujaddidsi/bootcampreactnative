//ES6
const readBooksPromise =  (time, book) => {
    console.log(`saya mulai membaca ${book.name}`)

    return new Promise((resolve, reject) => {
        setTimeout(function () {
            let sisaWaktu = 0
            if (time > book.timeSpent) {
                sisaWaktu = time - book.timeSpent
                console.log(`saya sudah membaca buku ${book.name}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            } else {
                console.log(`Saya sudah tidak punya waktu untuk baca ${book.name}`)
                reject(sisaWaktu)
            }
        }, book.timeSpent)
    })
}

module.exports = readBooksPromise